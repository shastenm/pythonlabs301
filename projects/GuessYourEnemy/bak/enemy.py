class Enemy:

    def __init__(self, dragon, guard, guard_dog):
        self.dragon = dragon
        self.guard = guard
        self.guard_dog = guard_dog

class Dragon(Enemy):
    
    def __init__(self, name, color, attack_power):
        self.name = name
        self.color = color
        self.attack_power = attack_power

blue_dragon = Dragon(None, blue, 10)
orange_dragon = Dragon(None, orange, 8)
purple_dragon = Dragon(None, purple, 7.5)
red_dragon = Dragon(None, red, 7)


class Guard(Enemy):

    def __init__(self, name, attack_power):
        self.name = name
        self.attack_power = attack_power

guard = Guard(None, 5)
officer = Guard(None, 6)
boxer = Guard(None, 7)
kung_fu_master = Guard(None, 7.5) 


class GuardDog:

    def __init__(self, name, breed, attack_power):
        self.name = name
        self.breed = breed
        self.attack_power = attack_power


 