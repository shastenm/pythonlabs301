from player import Player
import sys
players = []

def print_intro():
    print("""           Thanks for playing Guess Your Enemy. The rules are simple
            Choose doors 1-8, stand and fight the enemy or leave. 
            You will be able to nurish your body with food to increase your 
            health, you will score points for every enemy killed, you will get
            get the chance to  acquire more effective weapons for killing enemies. 
            You will also have the chance to score loot and buy things from the store at will.
            
            HINT: Different weapons are more useful for killing certain types of enemies 
            that other types of enemies so you will have the choice to choose which weapon you use. 
            
            The goal of this game is to see how many doors you can open, how many enemies
            you can slay, how much loot you can score, before losing all three lives. High Score wins. \n  \n 2""")
print_intro()

def get_game_instructions():
    pass
def create_players_num(x, y):        
    get_player_num = int(input("How many players (1 or 2)? "))
    return get_player_num

def enter_player(get_player_num):
    if get_player_num == 1:
        name = input("Please enter your first name: ")
        player = Player(None, None, 100, 3, name, 0, None)
        players.append(player)
        return player
    else:
        name = input("Please enter name for Player 1?: ")
        player = Player(None, None, 100, 3, name, 0, None)
        players.append(player)
        name = input("Please enter name for Player 2?: ")
        player = Player(None, None, 100, 3, name, 0, None)
        players.append(player)
        return player
p_num = create_players_num(1, 2)
if p_num == 1:
    p = enter_player(1)
    print(p)
    print(p.__dict__)
else:
    p = enter_player(2)
    print(p)
print(players)

def player_score():
    pass

def enter_room():
    pass

def engage_in_battle():
    pass

def leave_room():
    pass

def player_dies():
    while lives > 0:
        lives -= 1
    else:
        print("Game Over. ")

def enemy_dies():
    pass

def go_shopping():
    pass

def get_player_item():
    pass

