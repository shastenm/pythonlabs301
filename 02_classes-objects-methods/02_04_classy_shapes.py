# Create two classes that model a rectangle and a circle.
# The rectangle class should be constructed by length and width
# while the circle class should be constructed by radius.
#
# Write methods in the appropriate class so that you can calculate
# the area of both the rectangle and the circle, the perimeter
# of the rectangle, and the circumference of the circle.
import math

class Rectangle:
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def __str__(self):
        return f"The area of the rectangle is {self.length * self.width}"

area = Rectangle(5, 6)
print(area)

class Circle:
        
    def get_area(radius):
        cir_area = math.pi * (radius ** 2)
        return f"The area of a circle is {cir_area}."
    
    def get_circle_cir(radius):
        circumference_cir = 2 * (math.pi * radius)
        return f"The circumference of a circle is {circumference_cir}"

print(Circle.get_area(5))
print(Circle.get_circle_cir(5))

