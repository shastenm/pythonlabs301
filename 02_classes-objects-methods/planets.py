# Create a `Planet()` class that models attributes and methods of
# a planet object.
# Use the appropriate dunder method to get informative output with `print()`

class Planet:
    life_form = "unihabitable"
    def __init__(self, name, color, size, shape, moon,unique_characteristic):
        self.name = name
        self.color = color
        self.size = size
        self.shape = shape
        self.moon = moon
        self.unique_characteristic = unique_characteristic

    def __str__(self):
        return f"{self.name} is a {self.color} planet. It's size is {self.size} and has a {self.shape} shape. There are {self.moon} moons, and its {self.unique_characteristic} is what makes this planet stand out."

class Earth(Planet):
    life_form = "life-sustaining"

class Venus(Planet):
    pass

class Saturn(Planet):
    pass

earth = Planet("Earth", "blue", "average", "oblong", 1, "intelligent life-forms")
print(earth, f"Earth is {Earth.life_form}.")

venus = Planet("Venus", "beige", "small", "spherical", 0, "extreme heat")
print(venus, f"Venus is {Planet.life_form}.")

saturn = Planet("Saturn", "yellow", "large", "circular", 82, "rings")
print(saturn, f"Saturn is {Planet.life_form}.")

