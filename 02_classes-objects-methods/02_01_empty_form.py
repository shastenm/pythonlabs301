# A good way to think about how classes are blueprints of objects is to think of
# an empty form, for example one that you would get at a doctor's office.
# The empty form contains all the placeholders that define what information
# you need to fill to complete the form. If you fill it correctly, then you've
# successfully instantiated a form object, and your completed form now holds
# information that is specific to just you.
# Another patient's form will follow the same blueprint, but hold different info.
# You could say that every patient's filled form instance is part of the same
# empty form blueprint class that the doctor's office provided.
#
# Model such an application form as a Python class below, and instantiate
# a few objects from it.
class PatientForm:
    def __init__(self, name, age, sex, address, phone_number, history):
        self.name = name
        self.age = age
        self.sex = sex
        self.address = address
        self.phone_number = phone_number
        self.history = history

    def __str__(self):
        return f"{self.name} is a {self.age} year old {self.sex}, and lives at {self.address}. Their phone number is{self.phone_number}, and they have a history of {self.history}."

Mikey = PatientForm("Mikey", "37", "man", "123 Main Street", "(555)555-5555", "hyper-tension")
print(Mikey)
Valorey = PatientForm("Valorie", "45", "woman", "145 Short Street", "(456)123-6789", "anemia")
print(Valorey)
    